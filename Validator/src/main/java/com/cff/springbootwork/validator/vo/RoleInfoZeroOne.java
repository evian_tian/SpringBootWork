package com.cff.springbootwork.validator.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.cff.springbootwork.validator.custom.ZeroOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleInfoZeroOne {
	@NotNull(message = "roleId不能为空")
	private Integer roleId;
	
	@NotEmpty(message = "roleName不能为空")
	private String roleName;
	
	@ZeroOne(message = "deleted只能为0/1")
	private Integer deleted;
}
