package com.cff.springbootwork.validator.vo;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Negative;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserInfo {
	@Null(message = "创建时间不能填")
	@JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
	private Date createTime;
	
	@NotEmpty(message = "用户名不能为空")
	private String userName;

	@NotBlank(message = "姓名不能为空或空字符串")
	private String name;

	@Negative(message = "冬天温度在0°以下")
	private Integer temperatureWinter;
	
	@Positive(message = "夏天温度在0°以上")
	private Integer temperatureSummer;
	
	@Digits(integer = 11, message = "手机号是11位整数哦", fraction = 0)
	private String mobile;
	
	@NotNull(message = "年龄不能为空")
	@Min(value = 10, message = "年龄太小了")
	@Max(value = 35, message = "年龄太大了")
	private Integer age;

	@Size(min = 0, max = 2, message = "你女朋友个数在0-2之间")
	private List<String> girlFrinds;
	
	@Range(min = 0, max = 100, message = "你钱包里的钱在0-2之间")
	private Integer money;
	
	@Length(min = 4, max = 64, message = "地址在4-64之间")
	private String address;

	@AssertTrue(message = "对象必须是人")
	private Boolean people;

	@AssertFalse(message = "不能上来就删除")
	private Boolean delete;
	
	@Pattern(regexp="[0-9]{6}",message = "密码格式错误")
	private String password;

	@Email(message = "email格式错误")
	private String email;

	@JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
	@Future(message = "失效时间比当前时间晚")
	private Date expireTime;
	
	@JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
	@Past(message = "出生日期比当前时间早")
	private Date birthDate;
	
	@URL(message = "url填写错误")
	private String url;
}
