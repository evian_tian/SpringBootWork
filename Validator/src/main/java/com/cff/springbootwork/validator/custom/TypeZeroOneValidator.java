package com.cff.springbootwork.validator.custom;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TypeZeroOneValidator implements ConstraintValidator<ZeroOne, Object> {

	@Override
	public void initialize(ZeroOne constraintAnnotation) {

	}

	@Override
	public boolean isValid(Object obj, ConstraintValidatorContext context) {
		if (obj == null)
			return true;
		int curNum = 0;
		if (obj instanceof String) {
			String s = (String) obj;
			curNum = Integer.parseInt(s);
		} else if (obj instanceof Boolean) {
			boolean b = ((Boolean) obj).booleanValue();
			if (b) {
				curNum = 1;
			}
		} else if (obj instanceof Long) {
			curNum = ((Long) obj).intValue();
		} else {
			curNum = ((Integer) obj).intValue();
		}
		if (curNum == 0 || curNum == 1)
			return true;
		return false;
	}

}
