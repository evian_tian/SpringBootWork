package com.cff.springbootwork.validator.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleInfo {
	@NotNull(message = "roleId不能为空", groups=RoleGroup.class)
	private Integer roleId;
	
	@NotEmpty(message = "roleName不能为空", groups=RoleGroup.class)
	private String roleName;

}
