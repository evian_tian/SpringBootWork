package com.cff.springbootwork.validator.custom;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * 自定义类校验注解
 * 作用于类，用以校验0/1类型数据
 * @author cff
 *
 */
@Target(value = {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=TypeZeroOneValidator.class)
public @interface ZeroOne {
	
	String message() default "参数有误";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
