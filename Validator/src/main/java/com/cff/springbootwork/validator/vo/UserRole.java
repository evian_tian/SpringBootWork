package com.cff.springbootwork.validator.vo;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRole {
	@NotEmpty(message = "用户名不能为空")
	private String userName;
	
	@NotNull(message = "roleId不能为空")
	private Integer roleId;
	
	@Valid
	private UserInfo userInfo;
	
	private RoleInfo roleInfo;
}
