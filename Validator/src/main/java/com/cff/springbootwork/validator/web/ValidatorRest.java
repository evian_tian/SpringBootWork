package com.cff.springbootwork.validator.web;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.validator.vo.RoleGroup;
import com.cff.springbootwork.validator.vo.RoleInfo;
import com.cff.springbootwork.validator.vo.RoleInfoZeroOne;
import com.cff.springbootwork.validator.vo.UserInfo;
import com.cff.springbootwork.validator.vo.UserRole;
import com.cff.springbootwork.validator.vo.UserRoleInfo;

@RestController
@RequestMapping("/valid")
public class ValidatorRest {
	@Autowired
	Validator validator;

	@RequestMapping(value = "/test")
	public List<String> set(@Valid @RequestBody UserInfo userInfo, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			List<String> errorMsg = bindingResult.getAllErrors().stream().map(s -> s.getDefaultMessage())
					.collect(Collectors.toList());
			return errorMsg;
		}
		return Collections.singletonList("0000");
	}

	@RequestMapping(value = "/test1")
	public List<String> test1(@Valid @RequestBody UserRole userRole, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			List<String> errorMsg = bindingResult.getAllErrors().stream().map(s -> s.getDefaultMessage())
					.collect(Collectors.toList());
			return errorMsg;
		}
		return Collections.singletonList("0000");
	}

	@RequestMapping(value = "/test2")
	public List<String> test2(@RequestParam("roleId") Integer roleId, @RequestParam("roleName") String roleName) {
		RoleInfo roleInfo = new RoleInfo(roleId, roleName);
		Set<ConstraintViolation<RoleInfo>> sets = validator.validate(roleInfo);
		if (sets.isEmpty())
			return Collections.singletonList("0000");
		List<String> errorMsg = sets.stream().map(s -> s.getMessage()).collect(Collectors.toList());
		return errorMsg;
	}

	@RequestMapping(value = "/test3")
	public List<String> test3(@RequestParam("roleId") Integer roleId, @RequestParam("userName") String userName,
			@RequestParam("roleName") String roleName) {
		UserRoleInfo userRoleInfo = new UserRoleInfo();
		userRoleInfo.setRoleId(roleId);
		userRoleInfo.setUserName(userName);
		RoleInfo roleInfo = new RoleInfo(roleId, roleName);
		userRoleInfo.setRoleInfo(roleInfo);
		Set<ConstraintViolation<UserRoleInfo>> sets = validator.validate(userRoleInfo, RoleGroup.class, Default.class);
		if (sets.isEmpty())
			return Collections.singletonList("0000");
		List<String> errorMsg = sets.stream().map(s -> s.getMessage()).collect(Collectors.toList());
		return errorMsg;
	}
	
	@RequestMapping(value = "/test4")
	public List<String> test4(@Valid @RequestBody RoleInfoZeroOne roleInfoZeroOne, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			List<String> errorMsg = bindingResult.getAllErrors().stream().map(s -> s.getDefaultMessage())
					.collect(Collectors.toList());
			return errorMsg;
		}
		return Collections.singletonList("0000");
	}
}
