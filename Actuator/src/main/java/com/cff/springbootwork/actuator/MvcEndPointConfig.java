package com.cff.springbootwork.actuator;

import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnEnabledEndpoint;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cff.springbootwork.actuator.endpoint.SuperEndPoint;

@Configuration
@ServletComponentScan 
public class MvcEndPointConfig {

	@Bean
	@ConditionalOnEnabledEndpoint
	public SuperEndPoint superEndPoint() {
		return new SuperEndPoint();
	}
}