package com.cff.springbootwork.wallet.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.wallet.domain.WaItem;


@CacheConfig(cacheNames = "waItem")
@Transactional
public interface WaItemDao extends CrudRepository<WaItem, String>{
	@Cacheable
	List<WaItem> findByAcctType(String acctType);
}