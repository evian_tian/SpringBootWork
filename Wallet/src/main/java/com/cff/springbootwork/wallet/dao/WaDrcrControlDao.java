package com.cff.springbootwork.wallet.dao;

import javax.transaction.Transactional;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.wallet.domain.WaDrcrControl;


@CacheConfig(cacheNames = "waDrcrControl")
@Transactional
public interface WaDrcrControlDao extends CrudRepository<WaDrcrControl, String>{
	@Cacheable
	WaDrcrControl findByTransCode(String transCode);
}