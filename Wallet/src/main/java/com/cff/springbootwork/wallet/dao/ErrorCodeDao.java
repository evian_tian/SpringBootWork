package com.cff.springbootwork.wallet.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.wallet.domain.ErrorCode;


@Transactional
public interface ErrorCodeDao extends CrudRepository<ErrorCode, Long>{
	List<ErrorCode> findByErrCode(String errCode);
}