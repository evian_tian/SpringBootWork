package com.cff.springbootwork.wallet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.wallet.dao.WaProductDao;
import com.cff.springbootwork.wallet.domain.WaProduct;

@Service
public class WaProductService {
	
	@Autowired
	WaProductDao waProductDao;
	
	public WaProduct getProduct(String itemNo){
		List<WaProduct> list = waProductDao.findByItemNo(itemNo);
		if(list == null || list.size() <=0)return null;
		return list.get(0);
	}
}
