package com.cff.springbootwork.wallet.dao;

import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.wallet.domain.WaTransFlow;


public interface WaTransFlowDao extends CrudRepository<WaTransFlow, String>{
	WaTransFlow findByTransFlow(String transFlow);
}