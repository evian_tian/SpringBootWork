package com.cff.springbootwork.wallet.netty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.cff.springbootwork.wallet.netty.coder.TransRequestDecoder;
import com.cff.springbootwork.wallet.netty.coder.TransResponseEncoder;

import io.netty.channel.ChannelHandler;

@Component
public class TransSocketServer extends NettyServerTemplate{
	@Value("${transport}")
	private int port = 8888;
	@Autowired
	NettyServiceHandler nettyServerHandler;
	
	@Override
	public int getPort() {
		return port;
	}

	@Override
	public ChannelHandler[] createHandlers() {
		return new ChannelHandler[]{
//				new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()),
				new TransRequestDecoder(),
				nettyServerHandler,
				
				new TransResponseEncoder()
				};
	}

}
