package com.cff.springbootwork.wallet.dao;

import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.wallet.domain.WaAccountFlow;


public interface WaAccountFlowDao extends CrudRepository<WaAccountFlow, String>{
	WaAccountFlow findByAccFlow(String accFlow);
}