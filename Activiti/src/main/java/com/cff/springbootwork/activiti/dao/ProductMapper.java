package com.cff.springbootwork.activiti.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cff.springbootwork.activiti.domain.ProductTask;

@Mapper
public interface ProductMapper {

	public void save(ProductTask userTask);
	
	public List<ProductTask> getUserTask(String userid);
	
	public ProductTask getUserTaskByInstanceId(String instanceId);
	
	public void updateStatus(ProductTask userTask);

	public List<ProductTask> getUserTaskByCurrentViwer(String userid);
}
