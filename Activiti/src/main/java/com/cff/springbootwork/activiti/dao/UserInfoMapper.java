package com.cff.springbootwork.activiti.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cff.springbootwork.activiti.domain.UserInfo;

@Mapper
public interface UserInfoMapper {
	UserInfo findByUserName(@Param("userName") String userName);
	
}
