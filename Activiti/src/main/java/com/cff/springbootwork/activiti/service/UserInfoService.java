package com.cff.springbootwork.activiti.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.activiti.dao.UserInfoMapper;
import com.cff.springbootwork.activiti.domain.UserInfo;

@Service
public class UserInfoService {
	@Autowired
	UserInfoMapper userInfoDao;
	public UserInfo getUserInfoByUserName(String userName){
		return userInfoDao.findByUserName(userName);
	}
}
