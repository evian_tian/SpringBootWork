package com.cff.springbootwork.activiti.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;

public class CommitExecutionListener implements ExecutionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6482750935517963649L;

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		String eventName = execution.getEventName();
		System.out.println("BeforCommitExecutionListener:"+eventName);
		
		
	}

}
