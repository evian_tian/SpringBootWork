package com.cff.springbootwork.activiti.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.activiti.domain.ProductTask;
import com.cff.springbootwork.activiti.service.ProductService;

@RestController
@RequestMapping("/activiti")
public class ActivitiRest {

	@Autowired
	ProductService productService;

	@RequestMapping(value = "/add/{name}")
	public String add(@RequestBody ProductTask productTask, @PathVariable("name") String name) {
		productService.genTask(productTask, name);
		return "Success";
	}

	@RequestMapping(value = "/applyList/{name}")
	public List<ProductTask> applyList(@PathVariable("name") String name) throws Exception {
		return productService.applyList(name);
	}

	@RequestMapping(value = "/waitList/{name}")
	public List<ProductTask> waitList(@PathVariable("name") String name) throws Exception {
		return productService.waitList(name);
	}

	@RequestMapping(value = "/next/{name}")
	public Boolean processCommit(@PathVariable("name") String name, @RequestParam("instanceId") String instanceId,
			@RequestParam("taskId") String taskId) throws Exception {
		return productService.processCommit(taskId, instanceId, name);
	}
}
