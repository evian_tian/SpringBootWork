package com.cff.springbootwork.async.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.async.service.AsyncService;

/**
 * 测试乐异步任务
 * 
 * @author fufei
 *
 */
@RestController
@RequestMapping("/async")
public class AsyncRest {

	@Autowired
	AsyncService asyncService;

	@RequestMapping(value = "/thread", method = { RequestMethod.GET })
	public String thread(HttpServletRequest request) {
		request.setAttribute("test", "asdasd");
		asyncService.asyncThread();
		return "0000";
	}

	@RequestMapping(value = "/pool", method = { RequestMethod.GET })
	public String pool(HttpServletRequest request) {
		request.setAttribute("test", "asdasd");
		asyncService.asyncThreadPool();
		return "0000";
	}

	@RequestMapping(value = "/future", method = { RequestMethod.GET })
	public String future(HttpServletRequest request) {
		request.setAttribute("test", "asdasd");
		asyncService.asyncFuturePool();
		return "0000";
	}

	@RequestMapping(value = "/springOne", method = { RequestMethod.GET })
	public String springOne(HttpServletRequest request) {
		request.setAttribute("test", "asdasd");
		asyncService.asyncSpringAnnotationOne();
		return "0000";
	}

	@RequestMapping(value = "/springMuti", method = { RequestMethod.GET })
	public String springMuti(HttpServletRequest request) {
		request.setAttribute("test", "asdasd");
		asyncService.asyncSpringAnnotationMuti();
		return "0000";
	}
}
