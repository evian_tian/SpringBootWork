package com.cff.springbootwork.async.service;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Service;

@Service
public class ThreadPoolService {
	private ExecutorService executor;

	@PostConstruct
	public void init() {
//		executor = new ThreadPoolExecutor(20, 30, 60L, TimeUnit.SECONDS, new LinkedBlockingDeque<>(),
//				new ThreadPoolExecutor.AbortPolicy());
		
		executor = Executors.newCachedThreadPool();
	}

	public void execute(Runnable task) {
		executor.execute(task);
	}

	public <T> Future<T> submit(Callable<T> task) {
		return executor.submit(task);
	}

	@PreDestroy
	public void shutdown() {
		executor.shutdown();
	}
}
