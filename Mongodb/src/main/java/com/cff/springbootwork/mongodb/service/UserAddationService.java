package com.cff.springbootwork.mongodb.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.mongodb.dao.UserAddationDao;
import com.cff.springbootwork.mongodb.model.User;

/**
 * 第二种方式
 * 
 * @author fufei
 *
 */
@Service
public class UserAddationService {
	private static final Logger logger = LoggerFactory.getLogger(UserAddationService.class);
	@Autowired
	private UserAddationDao userAddationDao;

	/**
	 * 保存对象
	 *
	 * @param book
	 * @return
	 */
	public String save(User user) {
		logger.info("--------------------->[MongoDB save start]");
		user.setRegisterTime(new Date());
		userAddationDao.save(user);
		return "添加成功";
	}

	/**
	 * 查询所有
	 *
	 * @return
	 */
	public List<User> findAll() {
		logger.info("--------------------->[MongoDB find start]");
		return userAddationDao.findAll();
	}

	/***
	 * 根据id查询
	 * 
	 * @param id
	 * @return
	 */
	public User getUserById(Long id) {
		logger.info("--------------------->[MongoDB find start]");
		return userAddationDao.findById(id).orElse(null);
	}

	/**
	 * 根据名称查询
	 *
	 * @param name
	 * @return
	 */
	public User getUserByName(String name) {
		logger.info("--------------------->[MongoDB find start]");
		return userAddationDao.findByName(name);
	}

	/**
	 * 更新对象
	 *
	 * @param book
	 * @return
	 */
	public String update(User user) {
		logger.info("--------------------->[MongoDB update start]");
		userAddationDao.save(user);
		return "success";
	}

	/***
	 * 删除对象
	 * 
	 * @param book
	 * @return
	 */
	public String deleteUser(User user) {
		logger.info("--------------------->[MongoDB delete start]");
		userAddationDao.delete(user);
		return "success";
	}
}
