package com.cff.springbootwork.mongodb.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cff.springbootwork.mongodb.model.User;

public interface UserAddationDao extends MongoRepository<User, Long> {
	User findByName(String name);
}
