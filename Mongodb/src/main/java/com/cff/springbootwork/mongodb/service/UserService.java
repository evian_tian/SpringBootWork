package com.cff.springbootwork.mongodb.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.mongodb.model.User;

/**
 * 第一种方式
 * @author fufei
 *
 */
@Service
public class UserService {
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private MongoTemplate mongoTemplate;

	/**
	 * 保存对象
	 *
	 * @param book
	 * @return
	 */
	public String save(User user) {
		logger.info("--------------------->[MongoDB save start]");
		user.setRegisterTime(new Date());
		mongoTemplate.save(user);
		return "添加成功";
	}

	/**
	 * 查询所有
	 *
	 * @return
	 */
	public List<User> findAll() {
		logger.info("--------------------->[MongoDB find start]");
		return mongoTemplate.findAll(User.class);
	}

	/***
	 * 根据id查询
	 * 
	 * @param id
	 * @return
	 */
	public User getUserById(Long id) {
		logger.info("--------------------->[MongoDB find start]");
		Query query = new Query(Criteria.where("_id").is(id));
		return mongoTemplate.findOne(query, User.class);
	}

	/**
	 * 根据名称查询
	 *
	 * @param name
	 * @return
	 */
	public User getUserByName(String name) {
		logger.info("--------------------->[MongoDB find start]");
		Query query = new Query(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, User.class);
	}

	/**
	 * 更新对象
	 *
	 * @param book
	 * @return
	 */
	public String update(User user) {
		logger.info("--------------------->[MongoDB update start]");
		Query query = new Query(Criteria.where("_id").is(user.getId()));
		Update update = new Update().set("password", user.getPassword()).set("age", user.getAge()).set("phone",
				user.getPhone());
		// updateFirst 更新查询返回结果集的第一条
		mongoTemplate.updateFirst(query, update, User.class);
		return "success";
	}

	/***
	 * 删除对象
	 * 
	 * @param book
	 * @return
	 */
	public String deleteUser(User user) {
		logger.info("--------------------->[MongoDB delete start]");
		mongoTemplate.remove(user);
		return "success";
	}
}
