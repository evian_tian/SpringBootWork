package com.cff.springbootwork.elasticsearch.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.elasticsearch.domain.FQuestionElasticssearch;
import com.cff.springbootwork.elasticsearch.service.QuestionElasticsearchService;

@RestController
@RequestMapping("/elsearch")
public class ElasticsearchRest {

	@Autowired
	QuestionElasticsearchService questionElasticsearchService;

	@RequestMapping(value = "/test", method = { RequestMethod.GET })
	public List<FQuestionElasticssearch> test(@RequestParam(value = "value", required = false) String value) {
		return questionElasticsearchService.pageByOpenAndCatory(0, 10, "Spring专题", value).getContent();
	}
}
