package com.cff.springbootwork.caffeine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.mybatis.domain.UserInfo;
import com.cff.springbootwork.mybatis.service.UserInfoService;

@Service
@EnableCaching
public class CaffeineService {
	@Autowired
	UserInfoService userInfoService;

	@Cacheable(value = "userCache", key = "#root.targetClass.simpleName+'-'+#root.methodName+'-'+#userName")
	public UserInfo getUserInfoByUserName(String userName) {
		return userInfoService.getUserInfoByUserName(userName);
	}

	public UserInfoService getUserInfoService() {
		return userInfoService;
	}

	public void setUserInfoService(UserInfoService userInfoService) {
		this.userInfoService = userInfoService;
	}

}
