package com.cff.springbootwork.caffeine.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.caffeine.service.CaffeineService;
import com.cff.springbootwork.mybatis.domain.UserInfo;

@RestController
@RequestMapping("/caffeine")
public class CaffeineRest {

	@Autowired
	private CaffeineService caffeineService;

	@RequestMapping(value = "/test/{name}", method = { RequestMethod.GET })
	public UserInfo test(@PathVariable("name") String name) {
		return caffeineService.getUserInfoByUserName(name);
	}

}
