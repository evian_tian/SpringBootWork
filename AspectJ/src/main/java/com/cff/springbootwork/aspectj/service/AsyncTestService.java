package com.cff.springbootwork.aspectj.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cff.springbootwork.mybatis.domain.UserRole;
import com.cff.springbootwork.mybatis.service.UserRoleService;

@Service
public class AsyncTestService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private UserRoleService userRoleService;

	/**
	 * 测试
	 * 
	 * @param type
	 * @return
	 */
	@Async
	@Transactional(rollbackFor = Exception.class)
	public Integer getSyncTestInfo(int type) {
		logger.info("进入getTestInfo方法。");
		try {
			new Thread().sleep(10000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserRole userRole = new UserRole();
		userRole.setPhone("89789789789");
		userRole.setRole("43");
		userRole.setUserName("cff");
		userRoleService.saveTest(userRole);
		logger.info("保存成功");
		type = 2;
		return type;
	}
}
