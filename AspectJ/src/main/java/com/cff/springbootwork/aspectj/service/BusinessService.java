package com.cff.springbootwork.aspectj.service;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.aspectj.util.IPUtil;
import com.cff.springbootwork.dto.IpModel;

@Service
public class BusinessService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AsyncTestService asyncTestService;

	/**
	 * 获取ip地址
	 * 
	 * @param request
	 * @return
	 */
	public IpModel getIpInfo(HttpServletRequest request, int type) {
		logger.info("进入getIpInfo方法。");
		IpModel ipModel = new IpModel();
		ipModel.setClientIpAddress(IPUtil.getIpAddr(request));
		ipModel.setServerIpAddress(IPUtil.localIp());
		Integer retType = asyncTestService.getSyncTestInfo(1);
		logger.info("异步返回值是{}", retType);
		//int i = 1/0;
		return ipModel;
	}

	/**
	 * 测试
	 * 
	 * @param type
	 * @return
	 */
	public Integer getTestInfo(int type) {
		logger.info("进入getTestInfo方法。");
		return type;
	}
}
