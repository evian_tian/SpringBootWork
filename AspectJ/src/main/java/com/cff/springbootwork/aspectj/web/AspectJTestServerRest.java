package com.cff.springbootwork.aspectj.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.aspectj.service.BusinessService;
import com.cff.springbootwork.dto.IpModel;
import com.cff.springbootwork.dto.ResultModel;

@RestController
@RequestMapping("/asp")
public class AspectJTestServerRest {
	@Autowired
	private BusinessService businessService;

	@RequestMapping(value = "/ip", method = { RequestMethod.GET })
	public ResultModel welCome(HttpServletRequest request) {
		IpModel ipModel = businessService.getIpInfo(request, 1);
		return ResultModel.ok(ipModel);
	}

	@RequestMapping(value = "/test", method = { RequestMethod.GET })
	public ResultModel test(HttpServletRequest request) {
		Integer ipModel = businessService.getTestInfo(1);
		return ResultModel.ok(ipModel);
	}
}
