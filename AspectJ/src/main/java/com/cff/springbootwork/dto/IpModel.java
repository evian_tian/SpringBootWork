package com.cff.springbootwork.dto;

public class IpModel {
	private String clientIpAddress;
	private String serverIpAddress;

	public String getClientIpAddress() {
		return clientIpAddress;
	}

	public void setClientIpAddress(String clientIpAddress) {
		this.clientIpAddress = clientIpAddress;
	}

	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public void setServerIpAddress(String serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}

	@Override
	public String toString() {
		return "IpModel [clientIpAddress=" + clientIpAddress + ", serverIpAddress=" + serverIpAddress + "]";
	}
}
