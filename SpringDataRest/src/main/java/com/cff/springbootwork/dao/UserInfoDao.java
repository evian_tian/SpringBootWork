package com.cff.springbootwork.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.cff.springbootwork.domain.UserInfo;

/**
 * collectionResourceRel是json的最外层显示名称，path是访问路径
 * 
 * @author fufei
 *
 */
@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserInfoDao extends JpaRepository<UserInfo, String> {
	List<UserInfo> findByName(@Param("name") String name);

	/**
	 * 不暴漏删除接口
	 */
	@Override
	@RestResource(exported = false)
	void deleteById(String id);

	@Override
	void delete(UserInfo entity);
}
