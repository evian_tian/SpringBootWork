package com.cff.springbootwork.sharding.jdbc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cff.springbootwork.sharding.jdbc.domain.ChatInfo;

@Mapper
public interface ChatInfoMapper {
	@Select({
	   "<script>",
	        "SELECT ",
	        "chat_no as chatNo,user_id as userId,live_id as liveId,nick_name as nickName,create_time as createTime,delete_flag as deleteFlag,read_flag as readFlag",
	        "FROM t_chat_info",
	   "</script>"})
	List<ChatInfo> findAll();
	
	@Select({
	   "<script>",
            "SELECT ",
            "chat_no as chatNo,user_id as userId,live_id as liveId,nick_name as nickName,create_time as createTime,delete_flag as deleteFlag,read_flag as readFlag",
            "FROM t_chat_info",
            "<trim prefix=\" where \" prefixOverrides=\"AND\">",
            	"<if test='paramIn.chatNo != null '> and chat_no = #{paramIn.chatNo} </if>",
				"<if test='paramIn.userId != null '> and user_id = #{paramIn.userId} </if>",
				"<if test='paramIn.liveId != null '> and live_id = #{paramIn.liveId} </if>",
				"<if test='paramIn.nickName != null '> and nick_name = #{paramIn.nickName} </if>",
				"<if test='paramIn.createTime != null '> and create_time = #{paramIn.createTime} </if>",
				"<if test='paramIn.deleteFlag != null '> and delete_flag = #{paramIn.deleteFlag} </if>",
				"<if test='paramIn.readFlag != null '> and read_flag = #{paramIn.readFlag} </if>",
            "</trim>",
	   "</script>"})
	public ChatInfo findByCondition(@Param("paramIn") ChatInfo paramIn);
	
	@Insert({
	   "<script> ",
        	"INSERT INTO t_chat_info",
            "( chat_no,user_id,live_id,nick_name,create_time,delete_flag,read_flag ) ",
            " values ",
            "( #{chatNo},#{userId},#{liveId},#{nickName},#{createTime},#{deleteFlag},#{readFlag} ) ",
	   "</script>"})
	int save(ChatInfo item);
	
	@Update({
	   "<script>",
	        "update t_chat_info",
	        "<trim prefix=\"set\" suffixoverride=\",\"> ",
			"<if test='paramIn.userId != null '> user_id = #{paramIn.userId}, </if>",
			"<if test='paramIn.liveId != null '> live_id = #{paramIn.liveId}, </if>",
			"<if test='paramIn.nickName != null '> nick_name = #{paramIn.nickName}, </if>",
			"<if test='paramIn.createTime != null '> create_time = #{paramIn.createTime}, </if>",
			"<if test='paramIn.deleteFlag != null '> delete_flag = #{paramIn.deleteFlag}, </if>",
			"<if test='paramIn.readFlag != null '> read_flag = #{paramIn.readFlag}, </if>",
	        "</trim>",
	        "where chat_no = #{chatNo}",
	   "</script>"
	})
	int update(@Param("updateIn") ChatInfo updateIn, @Param("chatNo") Long chatNo);
	
	@Delete({
	   "<script>",
	        " delete from t_chat_info",
	        "<trim prefix=\" where \" prefixOverrides=\"AND\">",
	     	"<if test='paramIn.chatNo != null '> and chat_no = #{paramIn.chatNo} </if>",
			"<if test='paramIn.userId != null '> and user_id = #{paramIn.userId} </if>",
			"<if test='paramIn.liveId != null '> and live_id = #{paramIn.liveId} </if>",
			"<if test='paramIn.nickName != null '> and nick_name = #{paramIn.nickName} </if>",
			"<if test='paramIn.createTime != null '> and create_time = #{paramIn.createTime} </if>",
			"<if test='paramIn.deleteFlag != null '> and delete_flag = #{paramIn.deleteFlag} </if>",
			"<if test='paramIn.readFlag != null '> and read_flag = #{paramIn.readFlag} </if>",
	        "</trim>",
	   "</script>"
	})
	int delete(@Param("paramIn") ChatInfo paramIn);
	
	
}
