package com.cff.springbootwork.sharding.jdbc.domain;

import java.util.Date;

public class ChatInfo {

	private Long chatNo;
	private Integer userId;
	private Integer liveId;
	private String nickName;
	private Date createTime;
	private Integer deleteFlag;
	private Integer readFlag;

	public void setChatNo(Long chatNo) {
		this.chatNo = chatNo;
	}

	public Long getChatNo() {
		return chatNo;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getLiveId() {
		return liveId;
	}

	public void setLiveId(Integer liveId) {
		this.liveId = liveId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Integer deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Integer getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(Integer readFlag) {
		this.readFlag = readFlag;
	}

}