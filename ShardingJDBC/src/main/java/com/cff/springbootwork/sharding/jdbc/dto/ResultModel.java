package com.cff.springbootwork.sharding.jdbc.dto;

public class ResultModel {

	private String errorCode;
	private String message;
	private Object data;

	public ResultModel() {
	}

	public ResultModel(String errorCode) {
		this.errorCode = errorCode;
	}

	public ResultModel(String errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;
	}

	public ResultModel(String errorCode, String message, Object data) {
		this.errorCode = errorCode;
		this.message = message;
		this.data = data;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public static ResultModel ok() {
		ResultModel resultModel = new ResultModel("0000", "成功");
		return resultModel;
	}

	public static ResultModel ok(Object data) {
		ResultModel resultModel = new ResultModel("0000", "成功");
		resultModel.setData(data);
		return resultModel;
	}

	public static ResultModel error() {
		ResultModel resultModel = new ResultModel("1111", "失败");
		return resultModel;
	}

	public static ResultModel resultModel(String message) {
		ResultModel resultModel = new ResultModel("1111", message);
		return resultModel;
	}
}
