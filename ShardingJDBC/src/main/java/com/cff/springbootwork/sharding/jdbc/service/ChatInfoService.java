package com.cff.springbootwork.sharding.jdbc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.sharding.jdbc.dao.ChatInfoMapper;
import com.cff.springbootwork.sharding.jdbc.domain.ChatInfo;

@Service
public class ChatInfoService {
	@Autowired
	ChatInfoMapper chatInfoMapper;
	@Autowired
	SnowflakeIdGenerator snowflakeIdGenerator;

	public void save(ChatInfo tChatInfo) {
		tChatInfo.setChatNo(snowflakeIdGenerator.nextId());
		chatInfoMapper.save(tChatInfo);
	}

	public void delete(ChatInfo tChatInfo) {
		chatInfoMapper.delete(tChatInfo);
	}

	public void update(ChatInfo tChatInfo) {

		chatInfoMapper.update(tChatInfo, tChatInfo.getChatNo());
	}

	public List<ChatInfo> findAll() {
		return chatInfoMapper.findAll();
	}

	public ChatInfo findByChatNoAndLiveId(Integer liveId, Long id) {
		ChatInfo condition = new ChatInfo();
		condition.setChatNo(id);
		condition.setLiveId(liveId);
		return chatInfoMapper.findByCondition(condition);
	}

	public ChatInfo findById(Long id) {
		ChatInfo condition = new ChatInfo();
		condition.setChatNo(id);
		return chatInfoMapper.findByCondition(condition);
	}
}