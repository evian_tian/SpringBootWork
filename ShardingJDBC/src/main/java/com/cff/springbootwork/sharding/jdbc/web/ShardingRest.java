package com.cff.springbootwork.sharding.jdbc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.sharding.jdbc.domain.ChatInfo;
import com.cff.springbootwork.sharding.jdbc.dto.ResultModel;
import com.cff.springbootwork.sharding.jdbc.service.ChatInfoService;

@RestController
@RequestMapping("/sharing")
public class ShardingRest {

	@Autowired
	ChatInfoService chatInfoService;
	
	@RequestMapping(value = "/add", method = { RequestMethod.POST })
	public ResultModel add(@RequestBody ChatInfo chatInfo) {
		chatInfoService.save(chatInfo);
		return ResultModel.ok();
	}
	
	/**
	 * 查询也要携带分表字段，方便查找数据
	 * @param chatInfo
	 * @return
	 */
	@RequestMapping(value = "/info", method = { RequestMethod.GET })
	public ResultModel info(@RequestParam("liveId") Integer liveId, @RequestParam("chatNo") Long chatNo) {
		return ResultModel.ok(chatInfoService.findByChatNoAndLiveId(liveId,chatNo));
	}
	
	/**
	 * 不带分表字段查询
	 * @param chatInfo
	 * @return
	 */
	@RequestMapping(value = "/detail", method = { RequestMethod.GET })
	public ResultModel detail(@RequestParam("chatNo") Long chatNo) {
		return ResultModel.ok(chatInfoService.findById(chatNo));
	}
	
	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	public ResultModel delete(@RequestBody ChatInfo chatInfo) {
		chatInfoService.delete(chatInfo);
		return ResultModel.ok();
	}
	
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	public ResultModel update(@RequestBody ChatInfo chatInfo) {
		chatInfoService.update(chatInfo);
		return ResultModel.ok();
	}
}
