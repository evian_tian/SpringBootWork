Sharding-JDBC：

定位为轻量级Java框架，在Java的JDBC层提供的额外服务。 它使用客户端直连数据库，以jar包形式提供服务，无需额外部署和依赖，可理解为增强版的JDBC驱动，完全兼容JDBC和各种ORM框架。

适用于任何基于Java的ORM框架，如：JPA, Hibernate, Mybatis, Spring JDBC Template或直接使用JDBC。
基于任何第三方的数据库连接池，如：DBCP, C3P0, BoneCP, Druid, HikariCP等。
支持任意实现JDBC规范的数据库。目前支持MySQL，Oracle，SQLServer和PostgreSQL。

因为之前使用Spring-data-jpa和Sharding-JDBC结合使用过，所以这一篇选择mybatis结合Sharding-JDBC。

得出的结论如下：

1. sharding-jdbc不支持mysql-connector-java6.0以上版本，不得不降级到5.0版本。
2. sharding-jdbc不能实现主从同步，主从同步还是会依赖于数据库自身机制
3. RandomMasterSlaveLoadBalanceAlgorithm算法默认到从库列表中随机选择一个查询。
4. 更新时，查询到从库查询，更新到主库；
	如果从库有，主库无，会报乐观锁更新失败这种逗逼错误，大概是Jpa以为其他线程修改了主库。
	如果从库无，主库有，更新会提示主键重复，因为它是根据从库来判断是否存在这条记录的。
	两边一致后，可以正常更新，当然这个更新还只是更新主库。
5. 不带分表字段也能实现查询，但肯定是所有表扫描的，sharding-jdbc没打印日志，但jpa打印日志不同，增加了好几步。
6. 删除也是删除主库的，删除从库有主库无的记录会提示找不到记录的错误。删除必须带分表字段。提示错误：
	Parameter `null` should extends Comparable for sharding value.