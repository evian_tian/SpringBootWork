package com.cff.springbootwork.oauth.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.cff.springbootwork.oauth.handler.AjaxAuthFailHandler;
import com.cff.springbootwork.oauth.handler.AjaxAuthSuccessHandler;
import com.cff.springbootwork.oauth.handler.AjaxLogoutSuccessHandler;
import com.cff.springbootwork.oauth.handler.UnauthorizedEntryPoint;
import com.cff.springbootwork.oauth.provider.SimpleAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class ClientSecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private SimpleAuthenticationProvider simpleAuthenticationProvider;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(simpleAuthenticationProvider);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		SimpleUrlAuthenticationFailureHandler hander = new SimpleUrlAuthenticationFailureHandler();
		hander.setUseForward(true);
		hander.setDefaultFailureUrl("/login.html");

		http.exceptionHandling().authenticationEntryPoint(new UnauthorizedEntryPoint()).and().csrf().disable()
				.authorizeRequests().antMatchers("/oauth/**").permitAll().antMatchers("/mybatis/**").authenticated()
				.and().formLogin().loginPage("/login.html").usernameParameter("userName").passwordParameter("userPwd")
				.loginProcessingUrl("/login").successHandler(new AjaxAuthSuccessHandler())
				.failureHandler(new AjaxAuthFailHandler()).and().logout().logoutUrl("/logout")
				.logoutSuccessHandler(new AjaxLogoutSuccessHandler());

		http.authorizeRequests().antMatchers("/user/**").authenticated();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
