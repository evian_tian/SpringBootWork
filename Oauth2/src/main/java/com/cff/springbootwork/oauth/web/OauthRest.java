package com.cff.springbootwork.oauth.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.mybatis.domain.UserInfo;
import com.cff.springbootwork.mybatis.service.UserInfoService;

@RestController
@RequestMapping("/api")
public class OauthRest {

	@Autowired
	UserInfoService userInfoService;

	@RequestMapping(value = "/page", method = { RequestMethod.GET })
	public List<UserInfo> page() {
		return userInfoService.page(1, 10);
	}

}
