package com.cff.springbootwork.drools.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.drools.dto.ResultModel;
import com.cff.springbootwork.drools.service.RulesService;

/**
 * Drools的基本用法，动态规则+决策表
 */
@RequestMapping(value = "/rules")
@RestController
public class RulesController {

    @Autowired
    private RulesService rulesService;

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResultModel ruleAdd(@RequestParam String rule, @RequestParam String name) {
    	rulesService.setRule(name, rule);
        return ResultModel.ok();
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResultModel getRuleList() {
        return ResultModel.ok(rulesService.getRuleList());
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResultModel delete(@RequestParam Integer id) {
        return  ResultModel.ok(rulesService.deleteRule(id));
    }
    
    @RequestMapping(value = "/ruleUpdate", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResultModel ruleUpdate(@RequestParam String rule, @RequestParam Integer id, @RequestParam String name) {
        return  ResultModel.ok(rulesService.updateRule(id, name, rule));
    }
}

