package com.cff.springbootwork.drools.service;


import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderErrors;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.drools.dao.RulesDao;
import com.cff.springbootwork.drools.domain.Rules;
import com.cff.springbootwork.drools.domain.work.Shift;
import com.cff.springbootwork.drools.domain.work.WorkDate;
import com.cff.springbootwork.drools.domain.work.Worker;
import com.cff.springbootwork.drools.dto.ShiftRes;

@Service
public class ShiftService {
	@Autowired
	RulesDao rulesDao;

	/**
	 * 生成若woker及排期
	 * @param ruleName
	 * @return
	 * @throws Exception
	 */
	public List<ShiftRes> shiftExcute(String ruleName) throws Exception {
		Rules rules = rulesDao.getByName(ruleName);
		String rule = rules.getRule();
		List<WorkDate> lstDate = new ArrayList<>();
		for (int i = 1; i <= 31; i++) {
			lstDate.add(new WorkDate(i));
		}
		// 创建员工
		List<Worker> lstWorker = new ArrayList<>();
		int a = 0, b = 0;
		for (int i = 1; i <= 5; i++) {
			Worker w = new Worker(1, "司机" + (++a));
			w.setEasyDay(lstDate.size());
			lstWorker.add(w);
		}
		for (int i = 1; i <= 10; i++) {
			Worker w = new Worker(2, "外勤" + (++b));
			w.setEasyDay(lstDate.size());
			lstWorker.add(w);
		}
		KnowledgeBuilder kb = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kb.add(ResourceFactory.newByteArrayResource(rule.getBytes("utf-8")), ResourceType.DRL);

        // 检查规则正确性
        KnowledgeBuilderErrors errors = kb.getErrors();
        for (KnowledgeBuilderError error : errors) {
        	System.out.println("规则文件正确性有误：{}" + error);
            return null;
        }
        
        //从数据库动态获取的方法，因为已标注@Deprecated，这个地方考虑替换成其他方式
        KnowledgeBase kBase = KnowledgeBaseFactory.newKnowledgeBase();
        kBase.addKnowledgePackages(kb.getKnowledgePackages());
        
		KieSession ksession = kBase.newKieSession();
		for (WorkDate date : lstDate) {
			ksession.insert(date);
			for (Shift s : date.getShifts()) {
				ksession.insert(s);
			}
		}
		for (Worker worker : lstWorker) {
			ksession.insert(worker);
		}
		ksession.fireAllRules();
		ksession.dispose();
		return printWoker(lstWorker);
	}
	
	/**
	 * 返回排班表
	 * @param lstWorker
	 * @return
	 */
	public List<ShiftRes> printWoker(List<Worker> lstWorker) {
		List<Shift> lstShift = new ArrayList<>();
		List<ShiftRes> retList = new ArrayList<>();
		for (Worker w : lstWorker) {
			ShiftRes shiftRes = new ShiftRes();
			shiftRes.setWorker(w.getName());
			lstShift.clear();
			lstShift.addAll(w.getShifts());
			Collections.sort(lstShift);
			List<String> shiftList = new ArrayList<>();
			for (Shift shift : lstShift) {
				shiftList.add(String.format("%s日%s班", shift.getWorkDate().getDay(), shift.getNo()));
			}
			shiftRes.setShiftList(shiftList);
			retList.add(shiftRes);
		}
		
		return retList;
	}
}
