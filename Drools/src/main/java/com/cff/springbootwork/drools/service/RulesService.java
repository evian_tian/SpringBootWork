package com.cff.springbootwork.drools.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.drools.dao.RulesDao;
import com.cff.springbootwork.drools.domain.Rules;

@Service
public class RulesService {

    @Autowired
    private RulesDao rulesDao;


	public void setRule(String name, String rule) {
		rulesDao.setRule(name, rule);
	}


	public List<Rules> getRuleList() {
		return rulesDao.getRuleList();
	}


	public Integer deleteRule(Integer id) {
		return rulesDao.deleteRule(id);
	}


	public Integer updateRule(Integer id, String name, String rule) {
		return rulesDao.updateRule(id, name, rule);
	}
}
