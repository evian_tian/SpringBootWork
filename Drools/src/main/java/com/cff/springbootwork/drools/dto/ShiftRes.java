package com.cff.springbootwork.drools.dto;

import java.util.List;

public class ShiftRes {
	private String worker;
	private List<String> shiftList;

	public String getWorker() {
		return worker;
	}

	public void setWorker(String worker) {
		this.worker = worker;
	}

	public List<String> getShiftList() {
		return shiftList;
	}

	public void setShiftList(List<String> shiftList) {
		this.shiftList = shiftList;
	}

}
