package com.cff.springbootwork.drools.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.drools.dto.ResultModel;
import com.cff.springbootwork.drools.service.ShiftService;

/**
 * 排班
 */
@RequestMapping(value = "/shift")
@RestController
public class ShiftController {

	@Autowired
	private ShiftService shiftService;

	@RequestMapping(value = "/excute")
    public ResultModel excute(@RequestParam("name") String ruleName) throws Exception {
        return ResultModel.ok(shiftService.shiftExcute(ruleName));
    }
}
