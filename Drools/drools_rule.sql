/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50728
Source Host           : localhost:3306
Source Database       : cff

Target Server Type    : MYSQL
Target Server Version : 50728
File Encoding         : 65001

Date: 2019-12-04 10:27:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for drools_rule
-- ----------------------------
DROP TABLE IF EXISTS `drools_rule`;
CREATE TABLE `drools_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `rule` text CHARACTER SET utf8mb4,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `visible` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of drools_rule
-- ----------------------------
INSERT INTO `drools_rule` VALUES ('3', 'paiban', 'package com.cff.springbootwork.drools\r\n\r\nimport com.cff.springbootwork.drools.domain.work.*;\r\n\r\nrule \"司机\"\r\nwhen\r\n	shift : Shift(driver == null, $date : workDate, $no : no)\r\n	worker : Worker(type == 1, easyDay >= 6, maxDay <= 4, $total : shiftTotal )\r\n	eval( !shift.getWorkDate().containsWorker(worker) )\r\n	not Worker(type == 1, shiftTotal < $total )\r\n	not Shift(driver == null, workDate.day < $date.day)\r\n	not Shift(driver == null, workDate.day == $date.day, no < $no )\r\nthen\r\n	shift.setDriver( worker );\r\n	worker.addShift( shift );\r\n	update( shift );\r\n	update( worker );\r\nend\r\n\r\nrule \"外勤 1\"\r\nwhen\r\n	shift : Shift(assistant1 == null, $date : workDate, $no : no)\r\n	worker : Worker(easyDay >= 6, maxDay <= 4, $total : shiftTotal)\r\n	eval( !shift.getWorkDate().containsWorker(worker) )\r\n	not Worker( shiftTotal < $total )\r\n	not Shift(assistant1 == null, workDate.day < $date.day)\r\n	not Shift(assistant1 == null, workDate.day == $date.day, no < $no )\r\nthen\r\n	shift.setAssistant1( worker );\r\n	worker.addShift( shift );\r\n	update( shift );\r\n	update( worker );\r\nend\r\n\r\nrule \"外勤 2\"\r\nwhen\r\n	shift : Shift(assistant2 == null, $date : workDate, $no : no)\r\n	worker : Worker(easyDay >= 6, maxDay <= 4, total : shiftTotal)\r\n	eval( !shift.getWorkDate().containsWorker(worker) )\r\n	not Worker( shiftTotal < total )\r\n	not Shift(assistant2 == null, workDate.day < $date.day)\r\n	not Shift(assistant2 == null, workDate.day == $date.day, no < $no )\r\nthen\r\n	shift.setAssistant2( worker );\r\n	worker.addShift( shift );\r\n	update( shift );\r\n	update( worker );\r\nend\r\n\r\nrule \"移除班次\"\r\nwhen\r\n	shift : Shift()\r\n	eval( shift.isDone() )\r\nthen\r\n	retract( shift );\r\nend', '2017-01-17 10:43:14', '2019-12-03 17:41:24', '1');
