package com.cff.springbootwork.quartz.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import com.cff.springbootwork.quartz.service.ScheduleService;

@Component
public class SampleJob extends QuartzJobBean {
	@Autowired
	private ScheduleService scheduleService;

	private String name;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		scheduleService.doJob();
	}

	public ScheduleService getScheduleService() {
		return scheduleService;
	}

	public void setScheduleService(ScheduleService scheduleService) {
		this.scheduleService = scheduleService;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
