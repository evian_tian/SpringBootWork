package com.cff.springbootwork.quartz.config;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cff.springbootwork.quartz.job.SampleJob;

@Configuration 
public class QuartzJobConfig {
	@Value("${schedule.task.test}")
	private String testScheduleCron;
	
	
	@Bean
    public JobDetail teatQuartzDetail(){
        return JobBuilder.newJob(SampleJob.class).withIdentity("testQuartz").storeDurably().build();
    }

    @Bean
    public Trigger testQuartzTrigger(){
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(testScheduleCron);
        return TriggerBuilder.newTrigger().forJob(teatQuartzDetail())
                .withIdentity("testQuartz")
                .withSchedule(scheduleBuilder)
                .build();
    }
}
