package com.cff.springbootwork.mybatis.dao;

import org.apache.ibatis.annotations.Mapper;

import com.cff.springbootwork.mybatis.domain.UserInfoHis;

import cn.pomit.jpamapper.core.mapper.SimpleShardingMapper;

@Mapper
public interface UserInfoHisDao extends SimpleShardingMapper<UserInfoHis, String> {
	
}