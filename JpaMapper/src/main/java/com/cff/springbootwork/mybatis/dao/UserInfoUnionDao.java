package com.cff.springbootwork.mybatis.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.cff.springbootwork.mybatis.domain.UserInfoUnion;

import cn.pomit.jpamapper.core.mapper.CrudMapper;

@Mapper
public interface UserInfoUnionDao extends CrudMapper<UserInfoUnion, String> {
	List<UserInfoUnion> findByMobile(String mobile);
}