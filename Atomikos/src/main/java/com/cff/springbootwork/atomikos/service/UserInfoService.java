package com.cff.springbootwork.atomikos.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cff.springbootwork.atomikos.dao.master.UserInfoMasterDao;
import com.cff.springbootwork.atomikos.dao.slave.UserInfoSlaveDao;
import com.cff.springbootwork.atomikos.domain.UserInfo;

@Service
public class UserInfoService {
	@Autowired
	UserInfoMasterDao userInfoMasterDao;

	@Autowired
	UserInfoSlaveDao userInfoSlaveDao;

	public List<UserInfo> getUserInfoByUserName(String userName) {
		UserInfo userInfoMaster = userInfoMasterDao.findByUserName(userName);
		UserInfo userInfoSlave = userInfoSlaveDao.findByUserName(userName);

		List<UserInfo> list = new ArrayList<>();
		list.add(userInfoMaster);
		list.add(userInfoSlave);
		return list;
	}

	@Transactional(rollbackFor = Exception.class)
	public List<UserInfo> update(UserInfo userInfo) {
		userInfoMasterDao.update(userInfo);
		userInfoSlaveDao.update(userInfo);
		int i = 1 / 0;
		return getUserInfoByUserName(userInfo.getUserName());
	}
}
