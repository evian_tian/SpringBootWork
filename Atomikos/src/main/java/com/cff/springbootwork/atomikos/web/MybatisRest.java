package com.cff.springbootwork.atomikos.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.atomikos.domain.UserInfo;
import com.cff.springbootwork.atomikos.service.UserInfoService;

@RestController
@RequestMapping("/mybatis")
public class MybatisRest {

	@Autowired
	UserInfoService userInfoService;

	@RequestMapping(value = "/mybatis/{name}", method = { RequestMethod.GET })
	public List<UserInfo> testMybatis(@PathVariable("name") String name) {
		return userInfoService.getUserInfoByUserName(name);
	}
	
	@RequestMapping(value = "/update")
	public List<UserInfo> update(@RequestBody UserInfo userInfo) {
		return userInfoService.update(userInfo);
	}
}
