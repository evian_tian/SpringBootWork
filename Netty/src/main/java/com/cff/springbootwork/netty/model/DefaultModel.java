package com.cff.springbootwork.netty.model;

import com.alibaba.fastjson.JSONObject;

public class DefaultModel {
	public String title;
	public String content;
	public Integer type;
	public JSONObject data;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "DefaultModel [title=" + title + ", content=" + content + ", type=" + type + ", data=" + data + "]";
	}

}
