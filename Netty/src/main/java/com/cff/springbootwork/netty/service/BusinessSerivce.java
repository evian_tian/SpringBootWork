package com.cff.springbootwork.netty.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.cff.springbootwork.netty.model.DataObject;
import com.cff.springbootwork.netty.model.DefaultModel;

@Service
public class BusinessSerivce {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public String doBusiness(DefaultModel defaultMqModel) throws Exception {
		log.info(defaultMqModel.toString());

		if (defaultMqModel.getType() == 2) {
			DataObject dataObject = JSONObject.toJavaObject(defaultMqModel.getData(), DataObject.class);
			log.info(dataObject.getRemark());
		}
		return "0000";
	}
}
