package com.cff.springbootwork.netty.model;

public class DataObject {
	private String remark;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "DataObject [remark=" + remark + "]";
	}

}
