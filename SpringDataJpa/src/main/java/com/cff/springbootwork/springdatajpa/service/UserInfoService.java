package com.cff.springbootwork.springdatajpa.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cff.springbootwork.springdatajpa.dao.UserInfoDao;
import com.cff.springbootwork.springdatajpa.dao.UserInfoPageDao;
import com.cff.springbootwork.springdatajpa.domain.UserInfo;

@Service
public class UserInfoService {
	private Logger log = LoggerFactory.getLogger(OrderLogAspect.class);
	@Autowired
	UserInfoDao userInfoDao;

	@Autowired
	UserInfoPageDao userInfoPageDao;

	public UserInfo getUserInfoByUserName(String userName) {
		return userInfoDao.findByUserName(userName);
	}

	public void save(UserInfo entity) {
		userInfoDao.save(entity);
	}

	public void delete(String userName) {
		userInfoDao.deleteById(userName);
	}

	public void update(UserInfo entity) {
		userInfoDao.save(entity);
	}

	public List<UserInfo> page(int start, int size) {
		Pageable pageable = PageRequest.of(start, size);
		return userInfoPageDao.findAll(pageable).getContent();
	}
	
	public UserInfo transaction(String userName) {
		UserInfo userInfo = userInfoDao.findByMobile("8888888");
		log.info("内存地址：{},对象：{}",System.identityHashCode(userInfo),userInfo.toString());
		//userInfo.setUserName(userName);
		userInfo.setMobile("15607110725");
		userInfo.setName(userName);
		userInfo.setPasswd("asdasd");
		userInfoDao.save(userInfo);
		return userInfo;
	}
}
