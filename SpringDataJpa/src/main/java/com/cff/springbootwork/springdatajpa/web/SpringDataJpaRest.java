package com.cff.springbootwork.springdatajpa.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.springdatajpa.domain.UserInfo;
import com.cff.springbootwork.springdatajpa.service.UserInfoService;

@RestController
@RequestMapping("/springdatajpa")
public class SpringDataJpaRest {

	@Autowired
	UserInfoService userInfoService;

	@RequestMapping(value = "/springdatajpa/{name}", method = { RequestMethod.GET })
	public UserInfo springdatajpa(@PathVariable("name") String name) {
		return userInfoService.getUserInfoByUserName(name);
	}

	@RequestMapping(value = "/springdatajpa/page", method = { RequestMethod.GET })
	public List<UserInfo> page(@RequestParam("page") int page, @RequestParam("size") int size) {
		return userInfoService.page(page, size);
	}
	
	@RequestMapping(value = "/transaction", method = { RequestMethod.GET })
	public UserInfo transaction() {
		String userName = "cff";

		return userInfoService.transaction(userName);
	}
}
