package com.cff.springbootwork.springdatajpa.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.cff.springbootwork.springdatajpa.dao.UserInfoDao;
import com.cff.springbootwork.springdatajpa.domain.UserInfo;

@Aspect
@Component
public class OrderLogAspect {
	@Autowired
	UserInfoDao userInfoDao;
	private Logger log = LoggerFactory.getLogger(OrderLogAspect.class);


	@Transactional(rollbackFor = { Exception.class })
	@Around("execution(public * com.cff.springbootwork.springdatajpa.service.UserInfoService.transaction(..))")
	public Object doAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		Object[] objs = proceedingJoinPoint.getArgs();

		String userName = (String) objs[0];
		UserInfo userInfo = userInfoDao.findByUserName(userName );
		userInfo.setValid("9");
		
		log.info("内存地址：{},对象：{}",System.identityHashCode(userInfo),userInfo.toString());
		Object obj = proceedingJoinPoint.proceed();
		UserInfo userInfo2 = userInfoDao.findById(userName ).orElse(null);
		log.info("内存地址：{},对象：{}",System.identityHashCode(userInfo2),userInfo2.toString());
		log.info("内存地址：{},对象：{}",System.identityHashCode(userInfo),userInfo.toString());
		return obj;
	}

}
