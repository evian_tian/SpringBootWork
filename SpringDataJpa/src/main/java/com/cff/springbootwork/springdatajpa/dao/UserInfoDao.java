package com.cff.springbootwork.springdatajpa.dao;

import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.springdatajpa.domain.UserInfo;

public interface UserInfoDao extends CrudRepository<UserInfo, String>{
	
	UserInfo findByUserName(String userName);
	
	UserInfo findByMobile(String mobile);
}
