package com.cff.springbootwork.springdatajpa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.cff.springbootwork.springdatajpa.domain.UserInfo;

public interface UserInfoPageDao extends PagingAndSortingRepository<UserInfo, String>{
	
	Page<UserInfo> findByUserName(String userName,Pageable pageable);
}
