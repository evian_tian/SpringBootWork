package com.cff.springbootwork.ehcache.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.ehcache.service.EhcacheService;
import com.cff.springbootwork.mybatis.domain.UserInfo;

@RestController
@RequestMapping("/ehcache")
public class EhcacheRest {

	@Autowired
	private EhcacheService ehcacheService;

	@RequestMapping(value = "/test/{name}", method = { RequestMethod.GET })
	public UserInfo test(@PathVariable("name") String name) {
		return ehcacheService.getUserInfoByUserName(name);
	}

}
