package com.cff.springbootwork.token.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.cff.springbootwork.mybatis.domain.UserInfo;


public class TokenUserDetails extends UserInfo implements UserDetails {

	public TokenUserDetails(UserInfo appUser) {
		super(appUser);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6272869114201567325L;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return AuthorityUtils.createAuthorityList("USER");
	}
	
	@Override
	public String getUsername() {
		return super.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getPassword() {
		return super.getPasswd();
	}

}
