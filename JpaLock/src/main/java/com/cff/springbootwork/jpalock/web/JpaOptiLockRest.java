package com.cff.springbootwork.jpalock.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.jpalock.domain.UserInfo;
import com.cff.springbootwork.jpalock.service.UserInfoService;

/**
 * 测试乐观锁
 * @author fufei
 *
 */
@RestController
@RequestMapping("/jpalock")
public class JpaOptiLockRest {

	@Autowired
	UserInfoService userInfoService;

	@RequestMapping(value = "/detail/{name}", method = { RequestMethod.GET })
	public UserInfo detail(@PathVariable("name") String name) {
		return userInfoService.getUserInfoByUserName(name);
	}

	@RequestMapping(value = "/save")
	public String save(@RequestBody UserInfo userInfo) throws InterruptedException {
		userInfoService.save(userInfo);
		return "0000";
	}

	@RequestMapping(value = "/update/{time}")
	public String update(@RequestBody UserInfo userInfo, @PathVariable("time") int time) throws InterruptedException {
		userInfoService.updateWithTimeOptimistic(userInfo, time);

		return "0000";
	}

	@RequestMapping(value = "/update")
	public String update(@RequestBody UserInfo userInfo) throws InterruptedException {
		userInfoService.updateOptimistic(userInfo);
		return "0000";
	}
}
