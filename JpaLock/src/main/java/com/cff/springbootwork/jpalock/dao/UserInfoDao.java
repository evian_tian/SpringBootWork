package com.cff.springbootwork.jpalock.dao;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import com.cff.springbootwork.jpalock.domain.UserInfo;

public interface UserInfoDao extends CrudRepository<UserInfo, String> {
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	UserInfo findByUserName(String userName);
}
