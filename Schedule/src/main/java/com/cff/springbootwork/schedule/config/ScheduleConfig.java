package com.cff.springbootwork.schedule.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.cff.springbootwork.schedule.service.ScheduleService;

@Configuration
@EnableScheduling
public class ScheduleConfig {
	@Autowired
	ScheduleService scheduleService;

	@Scheduled(cron = "${schedule.task.test}")
	public void dayJob() {
		scheduleService.doJob();
	}
}
