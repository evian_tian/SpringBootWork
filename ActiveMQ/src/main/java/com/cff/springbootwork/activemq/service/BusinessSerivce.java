package com.cff.springbootwork.activemq.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.activemq.model.DefaultMqModel;
import com.cff.springbootwork.activemq.model.SeccondMqModel;

@Service
public class BusinessSerivce {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public void doBusiness(DefaultMqModel defaultMqModel) {
		log.info(defaultMqModel.toString());
		
		if(defaultMqModel.getType() == 2 && defaultMqModel instanceof SeccondMqModel){
			SeccondMqModel seccondMqModel = (SeccondMqModel) defaultMqModel;
			log.info(seccondMqModel.remark);
		}
	}
}
