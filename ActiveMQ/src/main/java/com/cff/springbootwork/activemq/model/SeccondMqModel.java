package com.cff.springbootwork.activemq.model;

public class SeccondMqModel extends DefaultMqModel {
	public String remark;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "SeccondMqModel [remark=" + remark + ", title=" + title + ", content=" + content + ", type=" + type
				+ "]";
	}

}
