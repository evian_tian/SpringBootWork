package com.cff.springbootwork.web;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.cff.springbootwork.service.TestFunctionService;

@Configuration
public class RoutingConfiguration {

	@Bean
	public RouterFunction<ServerResponse> monoRouterFunction(TestFunctionService testFunctionService) {
		return route(GET("/fun/get"), testFunctionService::get)
				.andRoute(POST("/fun/post").and(accept(MediaType.APPLICATION_FORM_URLENCODED)), testFunctionService::post)
				.andRoute(POST("/fun/json").and(accept(MediaType.APPLICATION_JSON)), testFunctionService::json);
	}
	
	@Bean
    public RouterFunction<ServerResponse> indexRouter(
        @Value("classpath:/static/index.html") final Resource indexHtml) {
        return route(GET("/"), request ->  ServerResponse.ok().contentType(MediaType.TEXT_HTML).syncBody(indexHtml));
    }

}
