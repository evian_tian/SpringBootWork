package com.cff.springbootwork.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.dto.ResultModel;
import com.cff.springbootwork.dto.web.TestEntity;

import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/pub")
public class WebRest {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/get")
	public ResultModel get(@RequestParam("type") String type) {
		
		return ResultModel.ok(type);
	}
	
	@RequestMapping(value = "/post")
	public ResultModel post(TestEntity testEntity) {
		
		return ResultModel.ok(testEntity);
	}
	
	@RequestMapping(value = "/json")
	public ResultModel json(@RequestBody TestEntity testEntity) {
		
		return ResultModel.ok(testEntity);
	}
	
	@RequestMapping(value = "/mono")
	public Mono<ResultModel> mono(@RequestBody TestEntity testEntity) {
		
		return Mono.just(ResultModel.ok(testEntity));
	}
}
