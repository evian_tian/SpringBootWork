package com.cff.springbootwork.service;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.cff.springbootwork.dto.web.TestEntity;

import reactor.core.publisher.Mono;

@Service
public class TestFunctionService {
	public Mono<ServerResponse> get(ServerRequest request) {
		String type = request.queryParam("type").orElse("NULL");
		return ServerResponse.ok().body(fromObject(type));
	}

	public Mono<ServerResponse> post(ServerRequest request) {
		Mono<MultiValueMap<String, String>> formData = request.formData();

		return formData.flatMap(map -> {
			return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromObject(map));
		});
	}

	public Mono<ServerResponse> json(ServerRequest request) {
		Mono<TestEntity> person = request.bodyToMono(TestEntity.class);
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).body(person, TestEntity.class);
	}
}
