package com.cff.springbootwork.thymeleaf.web;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cff.springbootwork.thymeleaf.dto.FQuestionInfo;
import com.cff.springbootwork.thymeleaf.dto.ResultCode;
import com.cff.springbootwork.thymeleaf.dto.ResultModel;

@RestController
@RequestMapping("/thymeleaf")
public class ThymeleafRest {
	@RequestMapping("/page")
	public <FQuestionIndex> ModelAndView getThymeleaf() {
		ModelAndView modelAndView = new ModelAndView("detail");

		try {
			FQuestionInfo fQuestionInfo = new FQuestionInfo();
			fQuestionInfo.setAuthor("cff");
			fQuestionInfo.setCatory("大爷");
			fQuestionInfo.setId(123123123L);
			fQuestionInfo.setOpen(1);
			fQuestionInfo.setStar(123);
			fQuestionInfo.setTitle("我就是一个测试模板引擎的实体而已。");

			fQuestionInfo.setCreateTime(new Date());

			ResultModel retOk = ResultModel.ok(fQuestionInfo);
			modelAndView.addObject("data", retOk);
		} catch (Exception e) {
			e.printStackTrace();
			modelAndView.addObject("data", new ResultModel(ResultCode.CODE_00004));
		}

		return modelAndView;
	}
}
