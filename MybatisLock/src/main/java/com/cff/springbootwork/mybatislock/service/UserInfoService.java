package com.cff.springbootwork.mybatislock.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cff.springbootwork.mybatislock.dao.UserInfoDao;
import com.cff.springbootwork.mybatislock.domain.UserInfo;

@Service
public class UserInfoService {
	@Autowired
	UserInfoDao userInfoDao;
	public UserInfo getUserInfoByUserName(String userName){
		return userInfoDao.findByUserName(userName);
	}

	public void save(UserInfo entity) {
		entity.setVersion(0);
		userInfoDao.save(entity);
	}
	@Transactional
	public void updateWithTimeOptimistic(UserInfo entity, int time) throws Exception {		
		UserInfo userInfo = userInfoDao.findByUserName(entity.getUserName());
		if (userInfo == null)
			return;

		if (!StringUtils.isEmpty(entity.getMobile())) {
			userInfo.setMobile(entity.getMobile());
		}
		if (!StringUtils.isEmpty(entity.getName())) {
			userInfo.setName(entity.getName());
		}
		Thread.sleep(time * 1000L);

		int ret = userInfoDao.updateWithVersion(userInfo);
		if(ret < 1)throw new Exception("乐观锁导致保存失败");
	}

	@Transactional
	public void updateOptimistic(UserInfo entity) throws Exception {
		UserInfo userInfo = userInfoDao.findByUserName(entity.getUserName());
		if (userInfo == null)
			return;

		if (!StringUtils.isEmpty(entity.getMobile())) {
			userInfo.setMobile(entity.getMobile());
		}
		if (!StringUtils.isEmpty(entity.getName())) {
			userInfo.setName(entity.getName());
		}

		int ret = userInfoDao.updateWithVersion(userInfo);
		if(ret < 1)throw new Exception("乐观锁导致保存失败");
	}

	@Transactional
	public UserInfo getUserInfoByUserNamePessimistic(String userName) {
		return userInfoDao.findByUserNameForUpdate(userName);
	}
	
	@Transactional
	public void updateWithTimePessimistic(UserInfo entity, int time) throws InterruptedException {		
		UserInfo userInfo = userInfoDao.findByUserNameForUpdate(entity.getUserName());
		if (userInfo == null)
			return;

		if (!StringUtils.isEmpty(entity.getMobile())) {
			userInfo.setMobile(entity.getMobile());
		}
		if (!StringUtils.isEmpty(entity.getName())) {
			userInfo.setName(entity.getName());
		}
		Thread.sleep(time * 1000L);

		userInfoDao.update(userInfo);
	}
	
	@Transactional
	public void updatePessimistic(UserInfo entity) {
		UserInfo userInfo = userInfoDao.findByUserNameForUpdate(entity.getUserName());
		if (userInfo == null)
			return;

		if (!StringUtils.isEmpty(entity.getMobile())) {
			userInfo.setMobile(entity.getMobile());
		}
		if (!StringUtils.isEmpty(entity.getName())) {
			userInfo.setName(entity.getName());
		}

		userInfoDao.update(userInfo);
	}

}
