package com.cff.springbootwork.mybatislock.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cff.springbootwork.mybatislock.domain.UserInfo;

@Mapper
public interface UserInfoDao {
	@Select({
		"<script>",
	        "SELECT ",
	        "user_name as userName,passwd,name,mobile,valid, user_type as userType, version as version",
	        "FROM user_info_test",
	        "WHERE user_name = #{userName,jdbcType=VARCHAR}",
	   "</script>"})
	UserInfo findByUserName(@Param("userName") String userName);
	
	@Select({
		"<script>",
	        "SELECT ",
	        "user_name as userName,passwd,name,mobile,valid, user_type as userType, version as version",
	        "FROM user_info_test",
	        "WHERE user_name = #{userName,jdbcType=VARCHAR} for update",
	   "</script>"})
	UserInfo findByUserNameForUpdate(@Param("userName") String userName);
	
	@Update({
        "<script>",
        " update user_info_test set",
        " name = #{name, jdbcType=VARCHAR}, mobile = #{mobile, jdbcType=VARCHAR},version=version+1 ",
        " where user_name=#{userName}",
        "</script>"
    })
    int update(UserInfo userInfo);
	
	@Update({
        "<script>",
        " update user_info_test set",
        " name = #{name, jdbcType=VARCHAR}, mobile = #{mobile, jdbcType=VARCHAR},version=version+1 ",
        " where user_name=#{userName} and version = #{version}",
        "</script>"
    })
    int updateWithVersion(UserInfo userInfo);

	@Insert({
        "<script>",
        "INSERT INTO user_info_test",
        "( user_name,",
        "name ,",
        "mobile,",
        "passwd,",
        "version",
         ") ",
        " values ",
         "( #{userName},",
         "#{name},",
         "#{mobile},",
         "#{passwd},",
         "#{version}",
        " ) ",
        "</script>"
    })
	int save(UserInfo entity);
}
