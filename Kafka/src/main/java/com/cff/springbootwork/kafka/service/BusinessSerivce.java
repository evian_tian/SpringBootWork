package com.cff.springbootwork.kafka.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.kafka.model.TransDataModel;

@Service
public class BusinessSerivce {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public TransDataModel doTrans(TransDataModel defaultMqModel) {
		log.info("处理消息{}", defaultMqModel);

		defaultMqModel.setType("1111");

		return defaultMqModel;
	}
}
