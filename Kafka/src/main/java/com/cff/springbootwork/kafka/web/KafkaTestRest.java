package com.cff.springbootwork.kafka.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.kafka.model.TransDataModel;
import com.cff.springbootwork.kafka.service.BusinessSerivce;

@RestController
@RequestMapping("/kafkaTest")
public class KafkaTestRest {
	@Autowired
	BusinessSerivce businessSerivce;

	@RequestMapping(value = "/test")
	public TransDataModel test(TransDataModel defaultMqModel) {
		return businessSerivce.doTrans(defaultMqModel);
	}

	@RequestMapping(value = "/test2")
	public TransDataModel test2(TransDataModel seccondMqModel) {
		return businessSerivce.doTrans(seccondMqModel);
	}
}
