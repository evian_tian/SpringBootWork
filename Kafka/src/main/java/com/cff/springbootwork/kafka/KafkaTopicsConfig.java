package com.cff.springbootwork.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//如果不存topics，通过NewTopic新建
@Configuration
public class KafkaTopicsConfig {

	@Bean
	public NewTopic logCenter() {
	    return new NewTopic("logCenter", 2, (short) 2);
	}

	@Bean
	public NewTopic logTest() {
	    return new NewTopic("logCenter_test", 2, (short) 2);
	}
}
