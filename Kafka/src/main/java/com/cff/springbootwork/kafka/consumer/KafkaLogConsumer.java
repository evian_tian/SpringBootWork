package com.cff.springbootwork.kafka.consumer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.cff.springbootwork.kafka.model.KafkaLogModel;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class KafkaLogConsumer {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@KafkaListener(topics = { "${kafka.topics.log}" })
	public void consumer(String message) {
		ObjectMapper mapper = new ObjectMapper();
		KafkaLogModel kafkaLogModel;
		try {
			kafkaLogModel = mapper.readValue(message, KafkaLogModel.class);
			log.info("收到消息：{}", kafkaLogModel.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
