package com.cff.springbootwork.rabbitmq.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cff.springbootwork.rabbitmq.dto.ParamReq;
import com.cff.springbootwork.rabbitmq.service.PomitDirectMessageService;
import com.cff.springbootwork.rabbitmq.service.PomitFanoutMessageService;
import com.cff.springbootwork.rabbitmq.service.PomitTopicMessageService;

@RestController
@RequestMapping("/rabbit")
public class RabbitWeb {
	@Autowired
	PomitDirectMessageService pomitDirectMessageService;
	
	@Autowired
	PomitFanoutMessageService pomitFanoutMessageService;
	
	@Autowired
	PomitTopicMessageService pomitTopicMessageService;

	@RequestMapping(value = "/direct", method = { RequestMethod.GET })
	public String direct() {
		ParamReq paramReq = new ParamReq();
		paramReq.setContent("asdasdxbcvbcb");
		paramReq.setMessageType("direct");
		pomitDirectMessageService.sendMessage(paramReq);
		return "0000";
	}
	
	@RequestMapping(value = "/fanout", method = { RequestMethod.GET })
	public String fanout() {
		ParamReq paramReq = new ParamReq();
		paramReq.setContent("asdasdxbcvbcb");
		paramReq.setMessageType("fanout");
		pomitFanoutMessageService.sendMessage(paramReq);
		return "0000";
	}
	
	@RequestMapping(value = "/topic1", method = { RequestMethod.GET })
	public String topic1() {
		ParamReq paramReq = new ParamReq();
		paramReq.setContent("asdasdxbcvbcb");
		paramReq.setMessageType("topic1");
		pomitTopicMessageService.sendMessage(paramReq);
		return "0000";
	}
	
	@RequestMapping(value = "/topic2", method = { RequestMethod.GET })
	public String topic2() {
		ParamReq paramReq = new ParamReq();
		paramReq.setContent("asdasdxbcvbcb");
		paramReq.setMessageType("topic2");
		pomitTopicMessageService.sendMessage2(paramReq);
		return "0000";
	}
}
