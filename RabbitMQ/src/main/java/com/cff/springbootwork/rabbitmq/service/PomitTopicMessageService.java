package com.cff.springbootwork.rabbitmq.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.rabbitmq.dto.ParamReq;

@Service
public class PomitTopicMessageService {
	@Value("${rabbit.topic.exchange}")
	private String rabbitTopicExchange;
	
	@Value("${rabbit.direct.key}")
	private String rabbitDirectKey;
	
	@Value("${rabbit.topic.key}")
	private String rabbitTopicKey;
	
	@Autowired
	AmqpTemplate amqpTemplateTopic;
	
	public void sendMessage(ParamReq paramReq){
		amqpTemplateTopic.convertAndSend(rabbitTopicExchange, rabbitDirectKey, paramReq);
	}
	
	public void sendMessage2(ParamReq paramReq){
		amqpTemplateTopic.convertAndSend(rabbitTopicExchange, rabbitTopicKey, paramReq);
	}
}
