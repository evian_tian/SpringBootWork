package com.cff.springbootwork.rabbitmq.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.rabbitmq.dto.ParamReq;

@Service
public class PomitFanoutMessageService {
	@Value("${rabbit.fanout.exchange}")
	private String rabbitFanoutExchange;
	@Autowired
	AmqpTemplate amqpTemplate;

	public void sendMessage(ParamReq paramReq) {
		amqpTemplate.convertAndSend(rabbitFanoutExchange, null, paramReq);
	}
}
