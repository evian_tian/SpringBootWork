package com.cff.springbootwork.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableRabbit
@Configuration
public class RabbitConfiguration {
	// 队列 分类配置
	@Value("${rabbit.direct.queue}")
	private String directQueueName;

	@Value("${rabbit.common.queue}")
	private String commonQueueName;

	@Value("${rabbit.common.queue2}")
	private String commonQueueName2;

	// key 分类配置
	@Value("${rabbit.direct.key}")
	private String rabbitDirectKey;

	@Value("${rabbit.common.queue.pattern}")
	private String rabbitCommonKeyPattern;

	@Value("${rabbit.common.queue2.pattern}")
	private String rabbitCommonKeyPattern2;

	// exchange 分类配置
	@Value("${rabbit.direct.exchange}")
	private String rabbitDirectExchange;

	@Value("${rabbit.fanout.exchange}")
	private String rabbitFanoutExchange;

	@Value("${rabbit.topic.exchange}")
	private String rabbitTopicExchange;

	// 队列声明
	@Bean(name = "directQueue")
	public Queue directQueue() {
		return new Queue(directQueueName);
	}

	@Bean(name = "commonQueueName")
	public Queue commonQueueName() {
		return new Queue(commonQueueName);
	}

	@Bean(name = "commonQueueName2")
	public Queue commonQueueName2() {
		return new Queue(commonQueueName2);
	}

	// exchange声明
	@Bean(name = "directExchange")
	public DirectExchange directExchange() {
		return new DirectExchange(rabbitDirectExchange);
	}

	@Bean(name = "fanoutExchange")
	public FanoutExchange fanoutExchange() {
		return new FanoutExchange(rabbitFanoutExchange);
	}

	@Bean(name = "topicExchange")
	public TopicExchange topicExchange() {
		return new TopicExchange(rabbitTopicExchange);
	}

	// 数据绑定声明
	@Bean(name = "directBinding")
	public Binding directBinding() {
		return BindingBuilder.bind(directQueue()).to(directExchange()).with(rabbitDirectKey);
	}

	@Bean(name = "fanoutBinding")
	public Binding fanoutBinding() {
		return BindingBuilder.bind(commonQueueName()).to(fanoutExchange());
	}

	@Bean(name = "fanoutBinding2")
	public Binding fanoutBinding2() {
		return BindingBuilder.bind(commonQueueName2()).to(fanoutExchange());
	}

	@Bean(name = "topicBinding1")
	public Binding topicBinding1() {
		return BindingBuilder.bind(commonQueueName()).to(topicExchange()).with(rabbitCommonKeyPattern);
	}

	@Bean(name = "topicBinding2")
	public Binding topicBinding2() {
		return BindingBuilder.bind(commonQueueName2()).to(topicExchange()).with(rabbitCommonKeyPattern2);
	}

	@Bean(name = "topicBinding3")
	public Binding topicBinding3() {
		return BindingBuilder.bind(directQueue()).to(topicExchange()).with(rabbitDirectKey);
	}

	// 数据转换声明
	@Bean
	public MessageConverter myMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	public SimpleRabbitListenerContainerFactory myFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer,
			ConnectionFactory connectionFactory) {
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		configurer.configure(factory, connectionFactory);
		factory.setMessageConverter(myMessageConverter());
		return factory;
	}
}
