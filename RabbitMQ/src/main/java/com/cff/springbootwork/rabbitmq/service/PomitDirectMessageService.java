package com.cff.springbootwork.rabbitmq.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cff.springbootwork.rabbitmq.dto.ParamReq;

@Service
public class PomitDirectMessageService {
	@Value("${rabbit.direct.key}")
	private String rabbitDirectKey;	
	@Value("${rabbit.direct.exchange}")
	private String rabbitDirectExchange;
	
	@Autowired
	AmqpTemplate amqpTemplate;
	
	public void sendMessage(ParamReq paramReq){
		amqpTemplate.convertAndSend(rabbitDirectExchange,rabbitDirectKey,paramReq);
	}
}
